set number
set nowrap
set bg=dark
set ts=4 sw=4 expandtab
set splitright
set fdm=indent
syntax on
set laststatus=2
set viminfo='50,<1000,s100,:0,n~/.vim/viminfo
set nocompatible              " be iMproved, required
filetype off                  " required
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'vim-airline/vim-airline'
Plugin 'mattn/emmet-vim'
Plugin 'scrooloose/nerdtree.git'
call vundle#end()
filetype plugin indent on

tnoremap <Esc> <C-\><C-n>

tnoremap <A-k> <C-\><C-n><C-w>k
tnoremap <A-j> <C-\><C-n><C-w>j
tnoremap <A-h> <C-\><C-n><C-w>h
tnoremap <A-l> <C-\><C-n><C-w>l
nnoremap <A-k> <C-w>k
nnoremap <A-j> <C-w>j
nnoremap <A-h> <C-w>h
nnoremap <A-l> <C-w>l
inoremap <A-k> <Esc><C-w>k
inoremap <A-j> <Esc><C-w>j
inoremap <A-h> <Esc><C-w>h
inoremap <A-l> <Esc><C-w>l
vnoremap <A-k> <Esc><C-w>k
vnoremap <A-j> <Esc><C-w>j
vnoremap <A-h> <Esc><C-w>h
vnoremap <A-l> <Esc><C-w>l

tnoremap <A-Up> <C-\><C-n><C-w>k
tnoremap <A-Down> <C-\><C-n><C-w>j
tnoremap <A-Left> <C-\><C-n><C-w>h
tnoremap <A-Right> <C-\><C-n><C-w>l
nnoremap <A-Up> <C-w>k
nnoremap <A-Down> <C-w>j
nnoremap <A-Left> <C-w>h
nnoremap <A-Right> <C-w>l
inoremap <A-Up> <Esc><C-w>k
inoremap <A-Down> <Esc><C-w>j
inoremap <A-Left> <Esc><C-w>h
inoremap <A-Right> <Esc><C-w>l
vnoremap <A-Up> <Esc><C-w>k
vnoremap <A-Down> <Esc><C-w>j
vnoremap <A-Left> <Esc><C-w>h
vnoremap <A-Right> <Esc><C-w>l
