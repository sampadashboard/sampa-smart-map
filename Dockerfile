FROM ruby:2.3.3
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /sampasmartmap
WORKDIR /sampasmartmap
ADD Gemfile /sampasmartmap/Gemfile
ADD Gemfile.lock /sampasmartmap/Gemfile.lock
ADD . /sampasmartmap
RUN bundle install
