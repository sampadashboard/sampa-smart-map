# Sampa Smart Map Group

* Felipe F. Laskoski
* Giuliano Belinassi
* Pedro Marcondes
* Victor Faria

# Roadmap

Project delivered iteratively in 4 releases.

# Release 1 - Admin side: Initiatives CRUD

In the first release, it's planned to be delivered the admin user interface 
side, where he can create, edit, view and delete new smart cities initiatives, 
all that using an One Page interface concept.

Issues:<br/>
[X] Join Initiatives Create and View pages<br/>
[X] Create initiatives Scaffold<br/>
[X] test Initiatives CRUD<br/>
[X] Data Verification<br/>
[X] Delete initiative<br/>
[X] Initiative Edit feature<br/>
[X] Initiative View<br/>
[X] Initiative Create Form<br/>

# Release 2 - MapManager

On that release, the intent is to deliver the interface responsible for yielding the initiatives data to an external API in a specific format.

Issues:<br/>
[X] Understand how to communicate with OSM<br/>
[X] Synthesize an abstract of how to use OSM<br/>
[X] Interface between DB and API OSM<br/>
[X] Interface between DB and external APIs<br/>
[X] Place OSM map on the page<br/>

# Release 3 - End User Side Interface and SearchEngine

The goal here was:
"to have the end user single page built and usable. It would 
contain the map showing the Smart Cities initiatives and with a search field for 
the user to look for a specific initiative or list of them in a specific region 
on the map"

However, in the october 31st class, professor told us to integrate the initiatives 
view with the Smart Cities Platform. But since the change decision was near the 
end of this release we decided and informed the monitor that, although it's not 
exactly as the current need of the customer, we will deliver the project
as planned before. That way, we avoid the risk of failing to deliver a working solution and 
we will have a functional implementation of the problem, which still will give 
considerable value to the customer.

Issues:<br/>

[X] unit tests and integration tests with capybara <br/>
[X] integration tests using BDD with cucumber <br/>
[X] Use continuous integration with GitLAb CI <br/>
[X] Bug Fix: Map preview is not generated after a initiative is added<br/>
[X] Bug Fix: Clarify about the database being built after the application<br/>

<br/>


<br/>
# Release 4 - Interface Enhancements and final Refactoring

 For this release we are going to add the initiatives database to the platform 
with the assistance of group 1 "plaftform support". This way, group 3 will be
able to show the initiatives on their map as well. 

Furthermore, We will remove the end user page and keep the admin page of our 
application, so that initiatives can be added.

We will make usability enhancements at both admin and end user pages. Also 
it will be time to absorb the feedbacks from the completed part of the system 
to improve its experience.<br/>

[X] Login Page
[X] "Authentication required" filter on create, edit and delete requests<br/>
[X] Login/Logout Buttons
[X] Collect feedbacks<br/>
[X] Make adjustments based on the feedbacks;<br/>
[X] Make javascript and others usability enhancements at the admin page<br/>
[X] Final refactoring<br/>
[X] Show dots on the map representing the coordinates of the initiatives<br/>
[X] Initiative preview when clicking on a dot<br/>
[X] Search function to find specific smart cities initiatives migrated to their application<br/>
[] Documentation review and update<br/>

With Group 1<br/>
[X] [Rejected] Smart Cities Initiatives in the USP platform   <br/>

<br/>
# API

API in OSM pattern


# INSTALLATION

INSTALLATION

To run this application, it is necessary mainly docker and docker-compose. You can find instructions for installing both applications at docker.com .

After docker and docker-compose are installed and its services are running on your machine, you need to build up the container by

> docker-compose build

To run the application, execute

> docker-compose up

The Rails application will then be ready at port 3000 on localhost.

The End User page will be available at the root URL

For the Administrator page, add "/initiatiaves" to the URL

# TESTS

Run:

> docker-compose run web rake

To execute both unit and integration tests.
