function formatDescription(initiative){
    description = '';
    description +=              initiative.name; 
    description += ' - '      + initiative.institution; 
    description += '<br>'     + initiative.address; 
    description += ', '       + initiative.city; 
    description += ', '       + initiative.state; 
    description += '<br><br>' + initiative.responsible; 
    description += '<br>'     + initiative.responsible_email; 
    description += '<br>'     + initiative.responsible_phone; 
    return description;
}

function resetMap(pins){
    var pracaDaSe        = {'lat': -23.55042565, 'lon': -46.6331223832318};
    var origin;

    if( pins[0] == undefined )
        origin = pracaDaSe;
    else
        origin           = {'lat': pins[0].latitude, 'lon': pins[0].longitude};

    var zoomDefaultLvl   = 13;
    var zoomMaxLvl       = 18;
    var tileURL          = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';
    var mapStyle         = 'mapbox.streets';
    var licenseSignature = 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ';
        licenseSignature += '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ';
        licenseSignature += 'Imagery © <a href="http://mapbox.com">Mapbox</a>';

    if(document.map !== undefined)
        document.map.remove();


    document.map = L.map('mapcontainer').setView(
                                            [origin.lat, origin.lon],
                                            zoomDefaultLvl
                                        );
    L.tileLayer(tileURL, {
        maxZoom:     zoomMaxLvl,
        attribution: licenseSignature,
        id:          mapStyle
    }).addTo(document.map);
}

function getPins(form, pinHandler_continuation){
  form = $(form);
  $.ajax({
     type: form.attr('method'),
     url:  form.attr('action'),
     data: { 
            'query': form.find('input[name="query"]').val()
           },
     success: pinHandler_continuation 
  });
} 

function replotPins(pins){
    resetMap(pins);
    plotPins(pins);
}

function plotPins(pins){
    $(pins).each(function(){
        L.marker([this.latitude, this.longitude])
            .addTo(document.map)
            .bindPopup(formatDescription(this));
    });
}

$(document).ready(function(){
    virtualInitiativeSearchForm = (function(){
        form = document.createElement('form');
        form.setAttribute('method', 'GET');
        form.setAttribute('action', '/filter');

        input = document.createElement('input');
        input.setAttribute('type', 'text');
        input.setAttribute('name', 'query');
        input.setAttribute('value', ''); // means "no filter"

        form.appendChild(input);
        return form;
    }());

    $('#search_initiative').on('submit', function(event){
        event.preventDefault();
        getPins(this, replotPins)
    });

    (function(){ // Initializing map with all initiatives
        getPins(virtualInitiativeSearchForm, replotPins);
    }());
});
