class Initiative < ApplicationRecord
  validates :name, presence: true
  validates :institution, presence:true
  validates :address, presence:true
  validates :city, presence:true
  validates :state, presence:true
  validates :responsible, presence:true
  validates :responsible_email, presence:true, :email_format => {:message => 'invalid e-mail address'}
  validates :responsible_phone, presence:true, numericality: { only_integer: true}, length: {minimum: 10, maximum: 15}

  geocoded_by      :get_full_address # can also be an IP address
  after_validation :geocode          # auto-fetch coordinates

  def get_full_address
     [address, city, state].compact.join(', ')
  end
end
