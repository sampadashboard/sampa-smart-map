json.extract! initiative, :id, :name, :institution, :address, :city, :state, :responsible, :responsible_email, :responsible_phone, :latitude, :longitude, :created_at, :updated_at
json.url initiative_url(initiative, format: :json)
