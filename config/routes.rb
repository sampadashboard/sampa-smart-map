Rails.application.routes.draw do
  devise_for :admins, controllers: {registrations: "registrations"}
  resources :initiatives
  root 'initiatives#main_page'
  get '/filter', to: 'initiatives#filter'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
