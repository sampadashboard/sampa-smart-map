class CreateInitiatives < ActiveRecord::Migration[5.0]
  def change
    create_table :initiatives do |t|
      t.string :name
      t.string :institution
      t.string :address
      t.string :city
      t.string :state
      t.string :responsible
      t.string :responsible_email
      t.string :responsible_phone
      t.float :latitude
      t.float :longitude
      
      t.timestamps
    end
  end
end
