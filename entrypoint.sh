#!/bin/bash
set -e

if [ -f sampasmartmap/tmp/pids/server.pid ]; then
	echo "Cleaning server PID"
	sudo rm -f sampasmartmap/tmp/pids/server.pid
fi

rake db:create
rake db:migrate
export RAILS_LOG_TO_STDOUT=1

exec "$@"
