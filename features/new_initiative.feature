Feature: new_initiative

Scenario:
	Given I am a administrator
	And I am in initiatives page
	When I click on Create New Initiative
	Then I should see the New Initiative page


Scenario:
	Given I am a administrator
	And I am in new_initiative page
	When I fill the initiative_name field with Poli-Eletrica-USP1
	And the initiative_institution field with USP
	And the initiative_address field with Av. Prof Luciando Gualberto, 380
	And the initiative_city field with São Paulo
	And the initiative_state field with SP
	And the initiative_responsible field with Hueraldo
	And the initiative_responsible_email field with hue@hue.com
	And the initiative_responsible_phone field with 1132311313
	When I press Create Initiative
	Then I should see Poli-Eletrica-USP1 in initiatives list
