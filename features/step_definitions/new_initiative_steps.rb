require 'pp'

Given /I am not logged in/ do

end

Given /that exists an administrator/ do
  user_email = 'administrator@smart.com'
  user_passw = '123456'
  usr = Admin.create!(email: user_email, password: user_passw)
end

Given /I am a (.+)/ do |user|
  user_email = user+'@smart.com'
  user_passw = '123456'
  usr = Admin.create!(email: user_email, password: user_passw)
  
  visit('admins/sign_in')
  fill_in 'admin_email', with: user_email
  fill_in 'admin_password', with: user_passw
  find('input[name="commit"]').click
end

And /I am in (.+) page/ do |page|
  visit(eval("#{page}_path"))
end

When /I click on (.+)/ do |link|
  click_link(link)
end

Then /I should see the (.+) page/ do |page|
  find('h1').should have_content(page)
end

Then /I should be in the (.+) page/ do |page|
  find('h2').should have_content(page)
end

When /I fill the (.+) field with (.+)/ do |field, value|
  fill_in field, with: value
end

And /^the (.+) field with (.+)/ do |field, value|
  fill_in field, with: value
end

When /^I press (.+)/ do |button|
  click_button(button)
end

Then /^I should see (.+) in initiatives list/ do |name|
  expect(page.text).to match(name)  
end
