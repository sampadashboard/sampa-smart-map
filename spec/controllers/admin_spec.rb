require 'rails_helper'
require 'helpers/stubs'

RSpec.describe RegistrationsController, type: :controller do

    it "Must allow only one admin" do
      as_admin
      Admin.create(email: 'hue@hue.com', password: 'xD')
      expect(Admin.count).to eq(1)
    end

    it "Should not override current admin" do
      as_admin
      adm = Admin.create(email: 'hue@hue.com', password: 'xD')
      expect(Admin.exists?(adm.id)).to be false
    end
end
