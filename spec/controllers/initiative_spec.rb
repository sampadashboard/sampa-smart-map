require 'rails_helper'
require 'helpers/stubs'

RSpec.describe InitiativesController, type: :controller do
    render_views
    before(:each) do
       @ime = Initiative.create!(get_ime_hash)
       get :show, params: {id: @ime.id}, format: :json
       @responde = response
    end

    it "Response with JSON" do
       json = JSON.parse(response.body)
       expect(json["id"]).to eq(@ime.id)
    end

    it "Checks if JSON has lat/lon values" do
       json = JSON.parse(response.body)
       expect(json).to have_key('latitude' )
       expect(json).to have_key('longitude')
    end
end
