require 'devise'

#parameters used in many test files

def get_ime_hash
	{    
		name: "TAPOO",    
		institution: "IME",    
		address: "1010 Rua do Matão",    
		city: "São Paulo",    
		state: "SP",    
		responsible: "Kon, Fábio",    
		responsible_email: "kon@ime.usp.br",    
		responsible_phone: "1130916498"     
	}    
end

def get_admin
  {
    email: "admin@admin.com",
    password: "123456"
  }
end

def as_admin
  admin = Admin.create(get_admin)
  sign_in admin
end
