require 'rails_helper'
require "validates_email_format_of/rspec_matcher"
require 'helpers/stubs'

RSpec.describe Initiative, type: :model do
    it { should validate_presence_of(:name) }

    it { should validate_presence_of(:institution) }

    it { should validate_presence_of(:address) }

    it { should validate_presence_of(:city) }

    it { should validate_presence_of(:state) }

    it { should validate_presence_of(:responsible) }   

    it { should validate_email_format_of(:responsible_email).with_message('invalid e-mail address') }

    it { should validate_presence_of(:responsible_phone) }

    it { should validate_numericality_of(:responsible_phone) }


    it "gets geolocation from address" do
         ime = Initiative.create!(get_ime_hash)
         expect(ime.latitude).to eq(-23.55926335)
         expect(ime.longitude).to eq(-46.7317499316745)
     end
end
