require 'rails_helper'
require 'pp'
require 'capybara/rspec'
require 'helpers/stubs'

RSpec.feature "AdminPageLayouts", type: :feature do
  scenario "GET admin_page" do
    visit initiatives_url
    expect(page).to have_link('Create New Initiative', :href => new_initiative_path)
  end
 
  scenario "GET new initiative page", type: :request do
    as_admin
    visit new_initiative_url
    expect(page).to have_link('Back', :href => initiatives_path)
    expect(page).to have_selector("input[type=submit][value='Create Initiative']")
  end
end
