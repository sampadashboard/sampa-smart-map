require 'rails_helper'
require 'pp'
require 'capybara/rspec'

RSpec.feature "EnduserPageLayouts", type: :feature do
  scenario "GET enduser page" do
    visit '/'
    expect(page).to have_selector("input#txt_user_query[type='text']")
    expect(page).to have_selector("input#btn_search")
  end
end
