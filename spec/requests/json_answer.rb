require 'rails_helper'
require 'pp'
require 'capybara/rspec'

RSpec.feature "EnduserPageLayouts", type: :feature do
  scenario "GET /enduser_page_layouts" do
    visit '/'
    expect(page).to have_selector("input#txt_user_query[type='text']")
    expect(page).to have_selector("button#btn_search")
  end
end
